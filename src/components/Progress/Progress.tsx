import React, {useState} from 'react'
import {block} from 'bem-cn'
import ProgressItem from './views/ProgressItem'
import './Progress.scss'

interface IProgressProps {
    steps: string[]
}

const MIN = 2
const MAX = 5

const Progress: React.FC<IProgressProps> = ({steps}) => {
    const b = block('Progress')
    const [currentStepIndex, setCurrentStepIndex] = useState<number>(0)

    if (!steps || steps.length === 0 || steps.length < MIN) {
        return null
    }

    steps = steps.slice(0, MAX)

    return (
        <div className={b()}>
            {steps.map((title, index) => (
                <ProgressItem
                    key={title}
                    title={title}
                    index={index}
                    currentStepIndex={currentStepIndex}
                    stepsLength={steps.length}
                    handleClick={() => setCurrentStepIndex(index)}
                />
            ))}
        </div>
    )
}

export default Progress