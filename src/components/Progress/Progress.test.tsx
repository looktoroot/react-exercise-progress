import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Progress from './Progress';

test('click Progress step', () => {
    render(
        <Progress steps={['One', 'Two', 'Three']} />
    );

    const activeClass = 'ProgressItem_is-step-active'
    const twoItem = screen.getAllByTestId('item')[1]
    const twoStep = screen.getAllByTestId('step')[1]

    expect(twoItem).not.toHaveClass(activeClass);

    fireEvent(
        twoStep,
        new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
        })
    )

    expect(twoItem).toHaveClass(activeClass);
});