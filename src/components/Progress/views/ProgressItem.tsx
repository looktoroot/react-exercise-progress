import React from 'react'
import {block} from 'bem-cn'
import './ProgressItem.scss'

interface ProgressItemProps {
    currentStepIndex: number
    stepsLength: number
    index: number
    title: string
    handleClick: () => void
}

const ProgressItem: React.FC<ProgressItemProps> = ({currentStepIndex, stepsLength, index, title, handleClick}) => {
    const b = block('ProgressItem')

    const isChangeAvailable = Math.abs(index - currentStepIndex) === 1
    const isStepActive = index <= currentStepIndex
    const isLineActive = index < currentStepIndex
    const isLast = index === stepsLength - 1
    const width = index === stepsLength - 1 ? null : 100 / (stepsLength - 1)

    return (
        <div
            data-testid={'item'}
            style={{width: width ? width + '%' : 'auto'}}
            className={b({
                'is-change-available': isChangeAvailable,
                'is-step-active': isStepActive,
                'is-line-active': isLineActive,
            })}
        >
            <div className={b('container')}>
                <div
                    data-testid={'step'}
                    role='button'
                    className={b('step')}
                    onClick={isChangeAvailable ? handleClick : () => {}}
                >
                    <span className={b('title')}>
                        {title}
                    </span>
                </div>
                {!isLast && (
                    <div className={b('line')}/>
                )}
            </div>
        </div>
    )
}

export default ProgressItem