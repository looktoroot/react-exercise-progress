import React from 'react'
import {block} from 'bem-cn'
import Progress from './components/Progress'
import './App.scss'

const App: React.FC = () => {
    const b = block('App')

    return (
        <div className={b()}>
            <div className={b('container')}>
                <Progress
                    steps={['Start']}
                />
                <Progress
                    steps={['Design', 'Build', 'Launch']}
                />
                <Progress
                    steps={['One', 'Two', 'Three', 'Four', 'Five', 'Six']}
                />
            </div>
        </div>
    )
}

export default App